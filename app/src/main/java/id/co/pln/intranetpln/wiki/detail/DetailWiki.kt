package id.co.pln.intranetpln.wiki.detail

import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import id.co.pln.intranetpln.R
import id.co.pln.intranetpln.config.APP_NAME
import id.co.pln.intranetpln.config.LOADING_TEXT
import id.co.pln.intranetpln.wiki.add.AddWiki
import org.sufficientlysecure.htmltextview.HtmlTextView

class DetailWiki : AppCompatActivity(),DetailWikiView {
    private lateinit var presenter: DetailWikiPresenter
    private lateinit var txt_title: TextView
    private lateinit var txt_content: HtmlTextView
    private lateinit var txt_last_edit: TextView
    private lateinit var btn_edit: Button
    private lateinit var loading: ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_wiki)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        txt_title= findViewById<TextView>(R.id.txt_title)
        txt_last_edit= findViewById<TextView>(R.id.txt_last_edit)
        txt_content= findViewById<HtmlTextView>(R.id.txt_content)
        btn_edit= findViewById<Button>(R.id.btn_edit)
        presenter= DetailWikiPresenter(this, DetailWikiInteractor(this))
        if(intent.hasExtra("uuid")){
            val uuidIntent: String?=intent.getStringExtra("uuid")
            presenter.loadWiki(uuidIntent!!)
        }else {
            val data: Uri?=intent.data
            presenter.searchWiki(data!!.getQueryParameter("text"))
        }
    }
    override fun showLoading() {
        loading= ProgressDialog.show(this, APP_NAME, LOADING_TEXT)
    }

    override fun hideLoading() {
        loading!!.dismiss()
    }
    override fun setWikiTitle(title: String) {
        setTitle(title)
        txt_title.setText(title)
    }

    override fun setWikiContent(content: String) {
        txt_content.setHtml(content)
    }

    override fun newWiki(text: String) {
        val i=Intent(this,AddWiki::class.java)
        i.putExtra("title",text)
        startActivity(i)
        finish()
    }

    override fun showAllComponent() {
        btn_edit.visibility= View.VISIBLE
        txt_last_edit.visibility= View.VISIBLE
        txt_title.visibility= View.VISIBLE
        txt_content.visibility= View.VISIBLE
    }

    override fun setLastEdit(text: String) {
        txt_last_edit.setText(text)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
