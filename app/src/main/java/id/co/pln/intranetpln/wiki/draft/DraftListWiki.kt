package id.co.pln.intranetpln.wiki.draft

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.MenuItem
import id.co.pln.intranetpln.R
import id.co.pln.intranetpln.config.APP_NAME
import id.co.pln.intranetpln.config.LOADING_TEXT
import id.co.pln.intranetpln.model.WikiPage
import id.co.pln.intranetpln.wiki.ui.WikiDraftListAdapter
import io.realm.RealmResults

class DraftListWiki : AppCompatActivity(),DraftListWikiView {
    private lateinit var presenter: DraftListWikiPresenter
    private lateinit var loading: ProgressDialog
    private lateinit var adapter: WikiDraftListAdapter
    private lateinit var viewManager: LinearLayoutManager
    private lateinit var list: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_draft_list_wiki)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        viewManager= LinearLayoutManager(this)
        list=findViewById(R.id.list_wiki) as RecyclerView
        list.layoutManager=viewManager
        presenter= DraftListWikiPresenter(this, DraftListWikiInteractor(this))
        presenter.loadDraftWiki()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun editDraft(text: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showLoading() {
        loading= ProgressDialog.show(this, APP_NAME, LOADING_TEXT)
    }

    override fun hideLoading() {
        loading!!.dismiss()
    }

    override fun loadList(data: RealmResults<WikiPage>) {
        adapter=WikiDraftListAdapter(data,this)
        list.adapter=adapter
    }
}
