package id.co.pln.intranetpln.wiki.draft

import id.co.pln.intranetpln.model.WikiPage
import io.realm.RealmResults

interface DraftListWikiView{
    fun editDraft(text: String)
    fun showLoading()
    fun hideLoading()
    fun loadList(data: RealmResults<WikiPage>)
}