package id.co.pln.intranetpln.wiki.detail

import android.util.Log
import id.co.pln.intranetpln.R
import id.co.pln.intranetpln.model.WikiPage
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern





class DetailWikiPresenter(
        val view: DetailWikiView,
        val interactor: DetailWikiInteractor
){

    fun loadWiki(uuid: String){
        view.showLoading()
        val wiki: WikiPage?=interactor.getWiki(uuid)
        loadContent(wiki!!.title,wiki,wiki!!.content)
    }
    fun searchWiki(text: String){
        view.showLoading()
        interactor.loadWikiText(text,{data->loadContent(text,data,null)})
    }
    fun loadContent(title: String,wiki: WikiPage?,text: String?){
        view.hideLoading()
        var data=""
        if(wiki!=null){
            data=wiki.content
        }
        if(text!=null){
            data=text
        }
        if(data.equals("")){
            view.newWiki(title)
        }else {
            val regex="^\\[\\[([a-zA-Z.\\\\\\/\\s\\t\\?\\>\\<\\;\\.\\,\\{\\}\\|\\-\\+\\(\\)\\*\\&\\^\\&\\%\\\$\\#\\@\\!\\~\\~0-9]+)\\]\\]"
            val pattern=Pattern.compile(regex,Pattern.MULTILINE)
            val matcher=pattern.matcher(data)
            while(matcher.find()){
                val linkTitle=matcher.group(0).replace("[[","").replace("]]","")
                Log.i("TITLE",linkTitle)
                data=data.replace("[[$linkTitle]]","<a href=\"app://id.co.pln.intranetpln.wiki?text=$linkTitle\">$linkTitle</a>")
            }
            data=data.replace("\n","<br/>")
            view.setWikiTitle(title)
            view.setWikiContent(data)
            var template=interactor.activity.resources.getString(R.string.wiki_last_edit)
            template=template.replace("[username]",wiki!!.userName)
            template=template.replace("[waktu]",SimpleDateFormat("dd MMM yyyy HH:mm:ss").format(Date(wiki!!.modifiedDate)))
            view.setLastEdit(template)
            view.showAllComponent()
        }

    }
}