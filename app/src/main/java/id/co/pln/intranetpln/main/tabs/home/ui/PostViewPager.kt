package id.co.pln.intranetpln.main.tabs.home.ui

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

class PostViewPager(fm: FragmentManager) : FragmentPagerAdapter(fm){
    override fun getItem(position: Int): Fragment {
        return when(position){
            0->{
                return TextPost()
            }
            1->{
                return PhotoVideoPost()
            }
            2->{
                return DocumentPost()
            }
            else -> {
                return TextPost()
            }
        }
    }

    override fun getCount(): Int {
       return 3
    }
}