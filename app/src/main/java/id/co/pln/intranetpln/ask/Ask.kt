package id.co.pln.intranetpln.ask


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.pln.intranetpln.R

class Ask : Fragment(),AskView {
    private lateinit var presenter: AskPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_ask, container, false)
        presenter=AskPresenter(this,AskInteractor(this))
        return view
    }


}
