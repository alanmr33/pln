package id.co.pln.intranetpln.wiki.draft

import id.co.pln.intranetpln.model.WikiPage
import org.json.JSONArray

class DraftListWikiPresenter(
    val view: DraftListWikiView,
    val interactor: DraftListWikiInteractor
){
    fun loadDraftWiki(){
        view.showLoading()
        interactor.getDraft({data->processData(data)})
    }
    fun processData(data: JSONArray?){
        view.hideLoading()
        interactor.getDB().beginTransaction()
        var list=interactor.getDB().createOrUpdateAllFromJson(WikiPage::class.java,data)
        interactor.getDB().commitTransaction()
        view.loadList(interactor.getDraft())
    }
}