package id.co.pln.intranetpln.wiki.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import id.co.pln.intranetpln.R;

public class WikiItemHollder extends RecyclerView.ViewHolder {
    public TextView txt_header,txt_item;

    public WikiItemHollder(@NonNull View itemView) {
        super(itemView);
        txt_header=itemView.findViewById(R.id.txt_header);
        txt_item=itemView.findViewById(R.id.txt_item);
    }
}
