package id.co.pln.intranetpln.login

interface LoginView {
    fun getUsername() : String
    fun getPassword() : String
    fun showLoading()
    fun hideLoading()
    fun showError(text: String)
    fun goToHome()
}