package id.co.pln.intranetpln.main.tabs.tasks


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.pln.intranetpln.R

class TaskFragment : Fragment(),TaskView {
    private lateinit var presenter: TaskPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_task, container, false)
        presenter=TaskPresenter(this,TaskInteractor(this))
        return view
    }


}
