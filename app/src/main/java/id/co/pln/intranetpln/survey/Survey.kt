package id.co.pln.intranetpln.survey


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.pln.intranetpln.R

class Survey : Fragment(),SurveyView {
    private lateinit var presenter: SurveyPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_survey, container, false)
        presenter=SurveyPresenter(this,SurveyInteractor(this))
        return view
    }


}
