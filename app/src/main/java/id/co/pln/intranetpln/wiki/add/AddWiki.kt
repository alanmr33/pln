package id.co.pln.intranetpln.wiki.add

import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import id.co.pln.intranetpln.R
import id.co.pln.intranetpln.config.APP_NAME
import id.co.pln.intranetpln.config.LOADING_TEXT

class AddWiki : AppCompatActivity(),WikiAddView {
    private lateinit var txt_title: EditText
    private lateinit var presenter: WikiAddPresenter
    private lateinit var txt_content: EditText
    private lateinit var txt_summary: EditText
    private lateinit var btn_add: Button
    var loading: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_wiki)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        txt_title=findViewById(R.id.txt_title) as EditText
        txt_content=findViewById(R.id.txt_content) as EditText
        txt_summary=findViewById(R.id.txt_summary) as EditText
        btn_add=findViewById(R.id.btn_publish) as Button
        presenter= WikiAddPresenter(this, WikiAddInteractor(this))
        if(intent.hasExtra("title")){
            setTitle(intent.getStringExtra("title"))
        }
        btn_add.setOnClickListener {
            presenter.addPage()
        }
    }

    override fun goBack() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun showLoading() {
        loading= ProgressDialog.show(this, APP_NAME, LOADING_TEXT)
    }

    override fun hideLoading() {
        loading!!.dismiss()
    }

    override fun showError(text: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun goToHome() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun setTitle(text: String) {
        txt_title.setText(text)
    }

    override fun setContent(text: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun goToList() {
        finish()
    }

    override fun getTitleWiki(): String {
        return txt_title.text.toString()
    }

    override fun getContentWiki(): String {
        return txt_content.text.toString()
    }

    override fun getSummaryWiki(): String {
        return txt_summary.text.toString()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
