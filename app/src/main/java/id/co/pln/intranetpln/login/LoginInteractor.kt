package id.co.pln.intranetpln.login

import android.app.Activity
import com.liferay.mobile.android.auth.SignIn.signIn
import com.liferay.mobile.android.auth.basic.BasicAuthentication
import com.liferay.mobile.android.callback.typed.JSONObjectCallback
import com.liferay.mobile.android.service.SessionImpl
import id.co.pln.intranetpln.App
import id.co.pln.intranetpln.config.SERVER_URL
import id.co.pln.intranetpln.repository.UserPrefRepository
import org.json.JSONObject
import java.lang.Exception

class LoginInteractor(
        val activity: Activity
){
    var userRepo: UserPrefRepository =UserPrefRepository(activity)
    var auth: BasicAuthentication?=null
    // DO LOGIN
    fun login(username : String,password : String,callback: (JSONObject?,Boolean)->Unit) {
        val app: App= activity.application as App
        auth =BasicAuthentication(username,password)
        app.session= SessionImpl(SERVER_URL,BasicAuthentication(username,password))
        signIn(app.session,object: JSONObjectCallback() {
            override fun onSuccess(result: JSONObject?) {
                callback(result, true)
            }

            override fun onFailure(exception: Exception?) {
                exception!!.printStackTrace()
                callback(null, false)
            }
        })
    }
    fun isLogin(): Boolean{
        return userRepo!!.isLogin()
    }
    fun readLogin(callback: (JSONObject?,Boolean)->Unit){
        val app: App= activity.application as App
        auth=userRepo.getAuth()
        app.session= SessionImpl(SERVER_URL,userRepo.getAuth())
        signIn(app.session,object: JSONObjectCallback() {
            override fun onSuccess(result: JSONObject?) {
                userRepo.setUserdata(result!!)
                callback(result, true)
            }

            override fun onFailure(exception: Exception?) {
                exception!!.printStackTrace()
                callback(null, false)
            }
        })
    }

}