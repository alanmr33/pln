package id.co.pln.intranetpln.wiki.add

import android.app.Activity
import com.liferay.mobile.android.callback.typed.JSONObjectCallback
import com.liferay.mobile.android.service.JSONObjectWrapper
import com.liferay.mobile.android.v7.wikipage.WikiPageService
import id.co.pln.intranetpln.App
import id.co.pln.intranetpln.config.*
import id.co.pln.intranetpln.repository.UserPrefRepository
import io.realm.Realm
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.sql.Date
import java.util.*

class WikiAddInteractor(
        val activity: Activity
){
    var realm: Realm?=null
    val app: App=activity.application as App
    val userRepo: UserPrefRepository = UserPrefRepository(activity)
    val wikiPageService: WikiPageService = WikiPageService(app!!.session)
    fun createPage(title: String,content: String,summary: String,callback: (JSONObject?)->Unit){
        app.session!!.callback=object : JSONObjectCallback(){
            override fun onSuccess(result: JSONObject?) {
                callback(result)
            }

            override fun onFailure(exception: Exception?) {
                exception!!.printStackTrace()
                callback(JSONObject())
            }

        }
//        val data=userRepo.getUserdata()
        val data=JSONObject()
        data.put("addGroupPermissions", true)
        data.put("addGuestPermissions", true)
        data.put("server", SERVER_URL)
        data.put("groupId", GROUP_ID)
        data.put("scopeGroupId", GROUP_ID)
        data.put("companyId", COMPANY_ID)
        data.put("userId", userRepo.getUserdata().getString("userId"))
        wikiPageService.addPage(WIKI_NODE_ID,title,content,summary,false, JSONObjectWrapper(data))
    }
}