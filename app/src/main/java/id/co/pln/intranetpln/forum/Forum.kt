package id.co.pln.intranetpln.forum


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.pln.intranetpln.R

class Forum : Fragment(),ForumView {
    private lateinit var presenter: ForumPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view=inflater.inflate(R.layout.fragment_forum, container, false)
        presenter=ForumPresenter(this,ForumInteractor(this))
        return view
    }


}
