package id.co.pln.intranetpln.wiki;

import android.support.v4.app.Fragment
import com.liferay.mobile.android.callback.typed.GenericCallback
import com.liferay.mobile.android.callback.typed.JSONArrayCallback
import com.liferay.mobile.android.service.JSONObjectWrapper
import com.liferay.mobile.android.v7.wikinode.WikiNodeService
import com.liferay.mobile.android.v7.wikipage.WikiPageService
import id.co.pln.intranetpln.App
import id.co.pln.intranetpln.config.GROUP_ID
import id.co.pln.intranetpln.config.WIKI_MAX
import id.co.pln.intranetpln.config.WIKI_NODE_ID
import id.co.pln.intranetpln.model.WikiPage
import id.co.pln.intranetpln.repository.UserPrefRepository
import io.realm.Realm
import io.realm.RealmResults
import org.json.JSONArray
import java.lang.Exception

class WikiInteractor(
    val fragment: Fragment
){
    private lateinit var realm: Realm
    val app: App?=fragment.activity!!.application as App
    val userRepo: UserPrefRepository= UserPrefRepository(fragment.activity!!)
    val wikiPageService: WikiPageService =WikiPageService(app!!.session)
    val wikiNodeService: WikiNodeService =WikiNodeService(app!!.session)
    fun getDraftCount(callback: (Int?) -> Unit){
        app!!.session!!.callback= object : GenericCallback<Int>() {
            override fun onSuccess(result: Int?) {
               callback(result)
            }

            override fun onFailure(exception: Exception?) {
                callback(0)
            }

            override fun transform(obj: Any?): Int {
                return obj.toString().toInt()
            }

        }
        wikiPageService.getRecentChangesCount(GROUP_ID, WIKI_NODE_ID)
    }
    fun getWikiCount(callback: (Int?) -> Unit){
//        app!!.session!!.callback=object: GenericCallback<Int>(){
//            override fun onSuccess(result: Int?) {
//                callback(result)
//            }
//
//            override fun onFailure(exception: Exception?) {
//                callback(0)
//            }
//
//            override fun transform(obj: Any?): Int {
//               return obj.toString().toInt()
//            }
//
//        }
//        wikiPageService.getPagesCount(GROUP_ID,
//                WIKI_NODE_ID,
//                false)
        //HARD CODE 1 LOOP
        callback(1)
    }
    fun getWiki(page: Int,callback: (JSONArray?)->Unit){
//        var start: Int?=0
//        if(page<=1){
//            start=0
//        }else{
//            start=((page-1)* WIKI_PER_PAGE)
//        }
//        val end: Int=start+ WIKI_PER_PAGE
        app!!.session!!.callback=object : JSONArrayCallback(){
            override fun onSuccess(result: JSONArray?) {
                callback(result)
            }

            override fun onFailure(exception: Exception?) {
                callback(JSONArray())
            }

        }
        //HARDCODE GET ALL NODE UP TO 1000
        wikiPageService.getNodePages(WIKI_NODE_ID, WIKI_MAX)
    }
    fun getDB(): Realm{
        Realm.init(fragment.context)
        realm= Realm.getDefaultInstance()
        return realm!!
    }
    fun getWikiList(): RealmResults<WikiPage>{
        Realm.init(fragment.context)
        realm= Realm.getDefaultInstance()
        return realm!!.where(WikiPage::class.java).equalTo("status","0".toLong())
                .sort("title").findAll()
    }
}
