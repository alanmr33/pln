package id.co.pln.intranetpln.main.tabs.notifications

interface NotificationView{
    fun showLoading()
    fun hideLoading()
    fun loadData()
}