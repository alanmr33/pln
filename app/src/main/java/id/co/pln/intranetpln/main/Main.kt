package id.co.pln.intranetpln.main

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewPager
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.SearchView
import android.widget.TextView
import id.co.pln.intranetpln.R
import id.co.pln.intranetpln.ask.Ask
import id.co.pln.intranetpln.forum.Forum
import id.co.pln.intranetpln.login.Login
import id.co.pln.intranetpln.main.ui.MainViewPagerAdapter
import id.co.pln.intranetpln.news.News
import id.co.pln.intranetpln.schedule.Schedule
import id.co.pln.intranetpln.search.Search
import id.co.pln.intranetpln.sharing.Sharing
import id.co.pln.intranetpln.survey.Survey
import id.co.pln.intranetpln.wiki.Wiki
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*

class Main : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, MainView {
    private lateinit var mainPresenter: MainPresenter
    private lateinit var mainInteractor: MainInteractor
    private var mainView: MainView =this
    private lateinit var side_email: TextView
    private lateinit var side_fullname: TextView
    private lateinit var search: SearchView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupHeader()
        setupToolbar()
        setupTabs()
        mainInteractor= MainInteractor(this)
        mainPresenter= MainPresenter(mainView,mainInteractor!!)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        drawer_layout.closeDrawer(GravityCompat.START)
        when(item.itemId){
            R.id.nav_home->{
                showTabMenu()
                goToHome()
            }
            R.id.nav_wiki->{
                setTitle(R.string.wiki)
                showPage()
                goToFragment(Wiki())
            }
            R.id.nav_forum->{
                setTitle(R.string.forum)
                showPage()
                goToFragment(Forum())
            }
            R.id.nav_ask->{
                setTitle(R.string.ask)
                showPage()
                goToFragment(Ask())
            }
            R.id.nav_agenda->{
                setTitle(R.string.schedule)
                showPage()
                goToFragment(Schedule())
            }
            R.id.nav_berita->{
                setTitle(R.string.news)
                showPage()
                goToFragment(News())
            }
            R.id.nav_survey->{
                setTitle(R.string.survey)
                showPage()
                goToFragment(Survey())
            }
            R.id.nav_bagi->{
                setTitle(R.string.sharing)
                showPage()
                goToFragment(Sharing())
            }
            R.id.nav_logout->{
                userLogout()
            }
        }
        return true
    }
    override fun search(text: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun refreshCount() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun goToHome() {
        view_main.setCurrentItem(0,true)
    }

    override fun goToNotif() {
        view_main.setCurrentItem(1,true)
    }

    override fun goToMessage() {
        view_main.setCurrentItem(2,true)
    }

    override fun goToTask() {
        view_main.setCurrentItem(4,true)
    }

    override fun goToProfile() {
        view_main.setCurrentItem(3,true)
    }

    override fun showTabMenu() {
        view_main.visibility= View.VISIBLE
        tabs.visibility=View.VISIBLE
        fragment_main.visibility=View.GONE
    }

    override fun showPage() {
        view_main.visibility= View.GONE
        tabs.visibility=View.GONE
        fragment_main.visibility=View.VISIBLE
    }

    override fun goToFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment_main,fragment)
                .commit()
    }

    override fun userLogout() {
        mainPresenter!!.logout()
        val intent= Intent(baseContext, Login::class.java)
        startActivity(intent)
        finish()
    }

    override fun setupTabs() {
        setTitle(R.string.home)
        view_main.adapter=MainViewPagerAdapter(supportFragmentManager)
        tabs.setupWithViewPager(view_main)
        tabs.getTabAt(0)!!.setIcon(R.drawable.ic_home)
        tabs.getTabAt(1)!!.setIcon(R.drawable.ic_notification)
        tabs.getTabAt(2)!!.setIcon(R.drawable.ic_message)
        tabs.getTabAt(3)!!.setIcon(R.drawable.ic_user)
        tabs.getTabAt(4)!!.setIcon(R.drawable.ic_tasks)
        view_main.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {

            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {

            }

            override fun onPageSelected(position: Int) {
                when(position){
                    0->{
                        setTitle(R.string.home)
                    }
                    1->{
                        setTitle(R.string.notification)
                    }
                    2->{
                        setTitle(R.string.profile)
                    }
                    3->{
                        setTitle(R.string.task)
                    }
                }
            }

        })
    }

    override fun setupHeader() {
        val nav_header=nav_view.getHeaderView(0)
        side_email=nav_header.findViewById(R.id.side_email) as TextView
        side_fullname=nav_header.findViewById(R.id.side_fullname) as TextView
    }

    override fun setupToolbar() {
        setSupportActionBar(toolbar)
        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
        search=findViewById(R.id.search_bar)
        search.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                val intent=Intent(baseContext,Search::class.java)
                intent.putExtra(SearchManager.QUERY,query)
                startActivity(intent)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }

        })
    }

    override fun setFullname(text: String) {
        side_fullname!!.text = text
    }

    override fun setEmail(text: String) {
        side_email!!.text=text
    }
}
