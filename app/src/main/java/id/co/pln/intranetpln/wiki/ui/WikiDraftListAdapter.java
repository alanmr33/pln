package id.co.pln.intranetpln.wiki.ui;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import id.co.pln.intranetpln.R;
import id.co.pln.intranetpln.model.WikiPage;
import io.realm.RealmResults;

public class WikiDraftListAdapter extends RecyclerView.Adapter<WikiItemHollder> {
    private RealmResults<WikiPage> wikiPages;
    private char lastChar;
    private Activity activity;

    public WikiDraftListAdapter(RealmResults<WikiPage> wikiPages, Activity activity) {
        this.wikiPages = wikiPages;
        this.activity = activity;
    }

    @NonNull
    @Override
    public WikiItemHollder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v=LayoutInflater.from(activity).inflate(R.layout.item_wiki,null,false);
        return new WikiItemHollder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull WikiItemHollder wikiItemHollder, int i) {
        WikiPage page=wikiPages.get(i);
        try {
            if (lastChar != page.getTitle().charAt(0)) {
                lastChar = page.getTitle().charAt(0);
                wikiItemHollder.txt_header.setVisibility(View.VISIBLE);
                wikiItemHollder.txt_header.setText(String.valueOf(lastChar));
            }
            wikiItemHollder.txt_item.setText(page.getTitle());
            wikiItemHollder.txt_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return wikiPages.size();
    }
}
