package id.co.pln.intranetpln.config

val SERVER_URL="http://116.254.101.249:8090/"
val PREF_NAME="PLN-INTRANET"
val DEBUG=true
val APP_NAME="INTRANET PORTAL"
val LOADING_TEXT="Loading ..."
val ERROR_INVALID_UP="Invalid Username/Password"
val COMPANY_ID: Long=20115
val GROUP_ID: Long=20142
val WIKI_NODE_ID: Long=31219
val WIKI_PER_PAGE=10
val WIKI_MAX=1000
val VERSION_L=70