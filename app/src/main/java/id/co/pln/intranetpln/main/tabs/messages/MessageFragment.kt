package id.co.pln.intranetpln.main.tabs.messages


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.pln.intranetpln.R

class MessageFragment : Fragment(),MessageView {
    private lateinit var presenter: MessagePresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_message, container, false)
        presenter=MessagePresenter(this,MessageInteractor(this))
        return view
    }


}
