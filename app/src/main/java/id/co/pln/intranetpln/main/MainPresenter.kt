package id.co.pln.intranetpln.main

import android.util.Log
import id.co.pln.intranetpln.config.DEBUG

class MainPresenter(
        val mainView: MainView,
        val mainInteractor: MainInteractor
){
    init {
        if(DEBUG){
            Log.i("USER","Email    : "+mainInteractor.getEmail())
            Log.i("USER","Fullname : "+mainInteractor.getFullname())
        }
        mainView.setEmail(mainInteractor.getEmail())
        mainView.setFullname(mainInteractor.getFullname())
    }
    fun logout(){
        mainInteractor.removeUserPref()
    }
}