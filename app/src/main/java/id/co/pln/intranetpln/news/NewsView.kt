package id.co.pln.intranetpln.news

interface NewsView{
    fun showLoading()
    fun hideLoading()
    fun getLastNews()
}