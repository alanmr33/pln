package id.co.pln.intranetpln.sharing

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.pln.intranetpln.R

class Sharing : Fragment(),SharingView {
    private lateinit var presenter: SharingPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_sharing, container, false)
        presenter=SharingPresenter(this,SharingInteractor(this))
        return view
    }

}
