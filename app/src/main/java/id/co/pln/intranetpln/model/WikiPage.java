package id.co.pln.intranetpln.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class WikiPage extends RealmObject {
    @PrimaryKey
    private String uuid;
    private Long companyId;
    private String content;
    private Long createDate;
    private String format;
    private Long groupId;
    private Boolean head;
    private Boolean minorEdit;
    private Long modifiedDate;
    private Long nodeId;
    private Long pageId;
    private String parentTitle;
    private String redirectTitle;
    private Long resourcePrimKey;
    private Long status;
    private Long statusByUserId;
    private String statusByUserName;
    private Long statusDate;
    private String summary;
    private String title;
    private Long userId;
    private String userName;
    private Double version;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public Boolean getHead() {
        return head;
    }

    public void setHead(Boolean head) {
        this.head = head;
    }

    public Boolean getMinorEdit() {
        return minorEdit;
    }

    public void setMinorEdit(Boolean minorEdit) {
        this.minorEdit = minorEdit;
    }

    public Long getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Long modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Long getNodeId() {
        return nodeId;
    }

    public void setNodeId(Long nodeId) {
        this.nodeId = nodeId;
    }

    public Long getPageId() {
        return pageId;
    }

    public void setPageId(Long pageId) {
        this.pageId = pageId;
    }

    public String getParentTitle() {
        return parentTitle;
    }

    public void setParentTitle(String parentTitle) {
        this.parentTitle = parentTitle;
    }

    public String getRedirectTitle() {
        return redirectTitle;
    }

    public void setRedirectTitle(String redirectTitle) {
        this.redirectTitle = redirectTitle;
    }

    public Long getResourcePrimKey() {
        return resourcePrimKey;
    }

    public void setResourcePrimKey(Long resourcePrimKey) {
        this.resourcePrimKey = resourcePrimKey;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public Long getStatusByUserId() {
        return statusByUserId;
    }

    public void setStatusByUserId(Long statusByUserId) {
        this.statusByUserId = statusByUserId;
    }

    public String getStatusByUserName() {
        return statusByUserName;
    }

    public void setStatusByUserName(String statusByUserName) {
        this.statusByUserName = statusByUserName;
    }

    public Long getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Long statusDate) {
        this.statusDate = statusDate;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getVersion() {
        return version;
    }

    public void setVersion(Double version) {
        this.version = version;
    }
}
