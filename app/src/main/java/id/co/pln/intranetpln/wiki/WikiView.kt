package id.co.pln.intranetpln.wiki

import id.co.pln.intranetpln.model.WikiPage
import io.realm.RealmResults

interface WikiView{
    fun goBack()
    fun setDraft(count: Int)
    fun loadData(data: RealmResults<WikiPage>)
    fun reload(data: RealmResults<WikiPage>)
    fun addPage()
    fun viewDraft()
    fun showLoading()
    fun hideLoading()
}