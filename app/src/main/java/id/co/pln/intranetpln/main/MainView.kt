package id.co.pln.intranetpln.main

import android.support.v4.app.Fragment

interface MainView{
    fun search(text : String)
    fun setupTabs()
    fun setupToolbar()
    fun refreshCount()
    fun goToHome()
    fun goToNotif()
    fun goToMessage()
    fun goToTask()
    fun goToProfile()
    fun showTabMenu()
    fun showPage()
    fun goToFragment(fragment: Fragment)
    fun userLogout()
    fun setFullname(text: String)
    fun setEmail(text: String)
    fun setupHeader()
}