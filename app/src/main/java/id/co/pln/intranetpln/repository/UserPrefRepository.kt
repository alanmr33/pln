package id.co.pln.intranetpln.repository

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.liferay.mobile.android.auth.basic.BasicAuthentication
import id.co.pln.intranetpln.config.PREF_NAME
import org.json.JSONObject

class UserPrefRepository(
        val activity: Activity
){
    val AUTH="auth"
    val USER_DATA_KEY="user_data"
    val IS_LOGIN_KEY="is_login"
    var preference: SharedPreferences? = null
    init {
        preference=activity.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }
    fun setIsLogin(is_login : Boolean){
        with(preference!!.edit()){
            putBoolean(IS_LOGIN_KEY,is_login)
            commit()
        }
    }
    fun isLogin(): Boolean{
        return preference!!.getBoolean(IS_LOGIN_KEY,false)
    }
    fun setUserdata(data: JSONObject){
        with(preference!!.edit()){
            putString(USER_DATA_KEY,data.toString())
            commit()
        }
    }
    fun getUserdata(): JSONObject{
        return JSONObject(preference!!.getString(USER_DATA_KEY,"{}"))
    }
    fun setAuth(auth: BasicAuthentication){
        with(preference!!.edit()){
            putString(AUTH,Gson().toJson(auth))
            commit()
        }
    }
    fun getAuth(): BasicAuthentication {
        return Gson().fromJson(preference!!.getString(AUTH,"{}"),BasicAuthentication::class.java)
    }
}