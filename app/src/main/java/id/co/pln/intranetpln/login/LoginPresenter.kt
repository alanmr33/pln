package id.co.pln.intranetpln.login

import id.co.pln.intranetpln.config.ERROR_INVALID_UP
import org.json.JSONObject

class LoginPresenter(
        val view: LoginView,
        val interactor: LoginInteractor
){
    fun checkAuth(){
        if(interactor.isLogin()){
            view.showLoading()
            interactor.readLogin({data,status->processResult(data,status)})
        }
    }
    fun authenticate(){
        view.showLoading()
        interactor.login(view.getUsername(),view.getPassword(),{data,status->processResult(data,status)})
    }
    fun processResult(data: JSONObject?,status: Boolean){
        view.hideLoading()
        if (status) {
            interactor.userRepo.setUserdata(data!!)
            interactor.userRepo.setIsLogin(true)
            interactor.userRepo.setAuth(interactor.auth!!)
            view.goToHome()
        } else {
            view.showError(ERROR_INVALID_UP)
        }
    }
}