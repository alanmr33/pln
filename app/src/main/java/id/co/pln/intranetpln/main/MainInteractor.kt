package id.co.pln.intranetpln.main

import android.util.Log
import id.co.pln.intranetpln.config.DEBUG
import id.co.pln.intranetpln.repository.UserPrefRepository
import org.json.JSONObject

class MainInteractor(
        val activity: Main
){
    private lateinit var userRepo: UserPrefRepository
    init {
         userRepo=UserPrefRepository(activity)
         if(DEBUG){
             Log.i("DATA","==========USER DATA==============")
             Log.i("DATA",userRepo!!.getUserdata().toString())
         }
    }
    fun getEmail(): String{
        return userRepo!!.getUserdata().getString("emailAddress")
    }
    fun getFullname(): String{
        return userRepo!!.getUserdata().getString("firstName")+" "+
                userRepo!!.getUserdata().getString("middleName")+" "+
                userRepo!!.getUserdata().getString("lastName")
    }
    fun removeUserPref(){
        userRepo!!.setIsLogin(false)
        userRepo!!.setUserdata(JSONObject())
    }
}