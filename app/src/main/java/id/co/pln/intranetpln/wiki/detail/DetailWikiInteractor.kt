package id.co.pln.intranetpln.wiki.detail

import android.app.Activity
import com.liferay.mobile.android.callback.typed.JSONObjectCallback
import com.liferay.mobile.android.v7.wikipage.WikiPageService
import id.co.pln.intranetpln.App
import id.co.pln.intranetpln.config.WIKI_NODE_ID
import id.co.pln.intranetpln.model.WikiPage
import id.co.pln.intranetpln.repository.UserPrefRepository
import io.realm.Realm
import org.json.JSONObject
import java.lang.Exception

class DetailWikiInteractor(
        val activity: Activity
){
    private lateinit var realm: Realm
    val app: App?=activity.application as App
    val userRepo: UserPrefRepository= UserPrefRepository(activity)
    val wikiPageService: WikiPageService = WikiPageService(app!!.session)
    fun getWiki(uuid: String): WikiPage?{
        Realm.init(activity)
        realm= Realm.getDefaultInstance()
        return realm!!.where(WikiPage::class.java).equalTo("uuid",uuid).findFirst()
    }
    fun loadWikiText(text: String,callback: (WikiPage?)->Unit){
        Realm.init(activity)
        realm= Realm.getDefaultInstance()
        val wiki: WikiPage?=realm!!.where(WikiPage::class.java).equalTo("title",text).findFirst()
        if(wiki==null) {
            app!!.session!!.callback = object : JSONObjectCallback() {
                override fun onSuccess(result: JSONObject?) {
                    realm!!.beginTransaction()
                    val newWiki: WikiPage?=realm!!.createOrUpdateObjectFromJson(WikiPage::class.java,result)
                    realm!!.commitTransaction()
                    callback(newWiki)
                }

                override fun onFailure(exception: Exception?) {
                    callback(null)
                }

            }
            wikiPageService.getPage(WIKI_NODE_ID, text)
        }else{
            callback(wiki)
        }
    }
}