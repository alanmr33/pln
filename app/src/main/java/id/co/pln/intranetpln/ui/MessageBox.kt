package id.co.pln.intranetpln.ui

import android.app.AlertDialog
import android.content.Context
import id.co.pln.intranetpln.config.APP_NAME

class MessageBox(
        val context: Context,
        val message: String
){
    fun show() {
        AlertDialog.Builder(context)
                .setTitle(APP_NAME)
                .setMessage(message)
                .setPositiveButton("OK"){dialog,which->{
                    dialog.dismiss()
                }}
                .create()
                .show()
    }
}