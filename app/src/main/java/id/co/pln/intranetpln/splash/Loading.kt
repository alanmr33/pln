package id.co.pln.intranetpln.splash

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.widget.Toast
import id.co.pln.intranetpln.R
import id.co.pln.intranetpln.login.Login
import id.co.pln.intranetpln.main.Main

class Loading : AppCompatActivity(),LoadingView {
    private var reqCode=1002
    private var allowAll: Boolean = false
    private var presenter: LoadingPresenter? = null
    private var permissions= arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading)
        presenter= LoadingPresenter(this)
        presenter!!.initCheck()
    }
    override fun checkPermission() {
        for (p in permissions){
            val permission = ContextCompat.checkSelfPermission(this,p)
            if (permission != PackageManager.PERMISSION_GRANTED) {
                allowAll=false
            }else{
                allowAll=true
            }
        }
        if(!allowAll){
            ActivityCompat.requestPermissions(this, permissions,reqCode)
        }
    }

    override fun goToHome() {
        val intent=Intent(baseContext, Main::class.java)
        startActivity(intent)
        finish()
    }

    override fun hasPermission(): Boolean {
        return allowAll
    }

    override fun goToLogin() {
        val intent=Intent(baseContext, Login::class.java)
        startActivity(intent)
        finish()
    }

    override fun showNotif() {
        Toast.makeText(baseContext,"allwo all permission",Toast.LENGTH_SHORT).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            reqCode -> {
                if(grantResults.size!=permissions.size){
                    Toast.makeText(this,"allow all permission",Toast.LENGTH_SHORT).show()
                }else{
                    presenter!!.initCheck()
                }
            }
        }
    }
}
