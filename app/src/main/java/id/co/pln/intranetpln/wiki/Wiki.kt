package id.co.pln.intranetpln.wiki


import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import id.co.pln.intranetpln.R
import id.co.pln.intranetpln.config.APP_NAME
import id.co.pln.intranetpln.config.LOADING_TEXT
import id.co.pln.intranetpln.model.WikiPage
import id.co.pln.intranetpln.wiki.ui.WikiItemAdapter
import io.realm.RealmResults
import id.co.pln.intranetpln.wiki.add.AddWiki
import id.co.pln.intranetpln.wiki.draft.DraftListWiki

class Wiki : Fragment(),WikiView {
    private lateinit var presenter: WikiPresenter
    private lateinit var loading: ProgressDialog
    private lateinit var adapter: WikiItemAdapter
    private lateinit var list_wiki: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var btn_draft: Button
    private lateinit var btn_add: Button
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_wiki, container, false)
        presenter=WikiPresenter(this,WikiInteractor(this))
        viewManager=LinearLayoutManager(activity)
        list_wiki=view.findViewById(R.id.list_wiki) as RecyclerView
        btn_draft=view.findViewById(R.id.btn_draft) as Button
        btn_draft!!.setOnClickListener {
            viewDraft()
        }
        btn_add=view.findViewById(R.id.btn_add)
        btn_add!!.setOnClickListener {
            addPage()
        }
        list_wiki!!.layoutManager=viewManager
        return view
    }

    override fun addPage() {
        val intent=Intent(activity, AddWiki::class.java)
        startActivity(intent)
    }

    override fun setDraft(count: Int) {
        btn_draft!!.setText("("+count+") "+resources.getString(R.string.draft))
    }

    override fun viewDraft() {
        val intent=Intent(activity, DraftListWiki::class.java)
        startActivity(intent)
    }

    override fun goBack() {

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter!!.loadWiki()
    }

    override fun showLoading() {
        loading= ProgressDialog.show(activity, APP_NAME, LOADING_TEXT)
    }

    override fun hideLoading() {
        loading!!.dismiss()
    }

    override fun loadData(data: RealmResults<WikiPage>) {
        Log.i("DATA","total wiki "+data.size)
        adapter=WikiItemAdapter(data,activity)
        list_wiki!!.adapter=adapter
    }

    override fun reload(data: RealmResults<WikiPage>) {
        loadData(data)
    }
}
