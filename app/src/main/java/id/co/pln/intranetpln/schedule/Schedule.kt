package id.co.pln.intranetpln.schedule


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.co.pln.intranetpln.R


class Schedule : Fragment(),ScheduleView {
    private lateinit var presenter: SchedulePresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_scheduler, container, false)
        presenter=SchedulePresenter(this,ScheduleInteractor(this))
        return view
    }


}
