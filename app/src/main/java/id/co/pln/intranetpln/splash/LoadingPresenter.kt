package id.co.pln.intranetpln.splash

class LoadingPresenter(
        view : LoadingView
){
    val view: LoadingView? = view
    fun initCheck(){
        view!!.checkPermission()
        if(view.hasPermission()){
            view.goToLogin()
        }else{
            view.showNotif()
        }
    }
}