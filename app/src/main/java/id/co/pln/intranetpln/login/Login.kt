package id.co.pln.intranetpln.login

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import id.co.pln.intranetpln.R
import id.co.pln.intranetpln.config.APP_NAME
import id.co.pln.intranetpln.config.LOADING_TEXT
import id.co.pln.intranetpln.main.Main
import id.co.pln.intranetpln.ui.MessageBox
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity(),LoginView {
    var presenter: LoginPresenter? = null
    var loading: ProgressDialog? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        getSupportActionBar()!!.hide()
        presenter= LoginPresenter(this, LoginInteractor(this))
        presenter!!.checkAuth()
        btn_login.setOnClickListener {
            presenter!!.authenticate()
        }
    }
    override fun getUsername(): String{
        return txt_usename.text.toString()
    }

    override fun getPassword(): String{
        return txt_password.text.toString()
    }

    override fun showLoading() {
        loading= ProgressDialog.show(this, APP_NAME, LOADING_TEXT)
    }

    override fun hideLoading() {
        loading!!.dismiss()
    }

    override fun showError(text: String) {
        MessageBox(baseContext!!,text)
    }

    override fun goToHome() {
        val intent= Intent(baseContext, Main::class.java)
        startActivity(intent)
        finish()
    }
}
