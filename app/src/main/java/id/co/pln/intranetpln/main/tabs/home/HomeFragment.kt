package id.co.pln.intranetpln.main.tabs.home


import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import id.co.pln.intranetpln.main.tabs.home.ui.PostViewPager
import id.co.pln.intranetpln.R

class HomeFragment : Fragment() {
    private var tabpost: TabLayout?=null
    private var pager: ViewPager?=null
    private var tabIcon1: View?=null
    private var tabIcon2: View?=null
    private var tabIcon3: View?=null
    private var iconText1: TextView?=null
    private var iconText2: TextView?=null
    private var iconText3: TextView?=null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val viewMain=inflater.inflate(R.layout.fragment_home, container, false)
        setupTabPost(viewMain,inflater)
        return viewMain
    }
    fun setupTabPost(viewMain: View,inflater: LayoutInflater){
        tabpost=viewMain.findViewById(R.id.tabpost) as TabLayout
        pager=viewMain.findViewById(R.id.post_pager) as ViewPager
        pager!!.adapter=PostViewPager(childFragmentManager)
        tabpost!!.setupWithViewPager(pager)

        tabIcon1=inflater.inflate(R.layout.layout_post_tab,null)
        iconText1=tabIcon1!!.findViewById(R.id.tabContent) as TextView
        iconText1!!.setText(R.string.make_a_post)
        tabpost!!.getTabAt(0)!!.setCustomView(tabIcon1)
        tabIcon2=inflater.inflate(R.layout.layout_post_tab,null)
        iconText2=tabIcon2!!.findViewById(R.id.tabContent) as TextView
        iconText2!!.setText(R.string.photo_video)
        iconText2!!.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_photo, 0, 0, 0)
        tabpost!!.getTabAt(1)!!.setCustomView(tabIcon2)
        tabIcon3=inflater.inflate(R.layout.layout_post_tab,null)
        iconText3=tabIcon3!!.findViewById(R.id.tabContent) as TextView
        iconText3!!.setText(R.string.document)
        iconText3!!.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_document, 0, 0, 0)
        tabpost!!.getTabAt(2)!!.setCustomView(tabIcon3)
    }

}
