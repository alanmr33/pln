package id.co.pln.intranetpln.main.tabs.home

import android.support.v4.app.Fragment
import com.liferay.mobile.android.v7.socialactivity.SocialActivityService
import id.co.pln.intranetpln.App
import io.realm.Realm

class HomeInteractor(
        val fragment: Fragment
){
    var realm: Realm?=null
    val app: App?=fragment.activity!!.application as App
    val service: SocialActivityService = SocialActivityService(app!!.session)
    fun getWall(){

    }
}