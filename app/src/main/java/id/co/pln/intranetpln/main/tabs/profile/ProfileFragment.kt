package id.co.pln.intranetpln.main.tabs.profile


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.pln.intranetpln.R

class ProfileFragment : Fragment(),ProfileView {
    private lateinit var presenter: ProfilePresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view=inflater.inflate(R.layout.fragment_profile, container, false)
        presenter=ProfilePresenter(this,ProfileInteractor(this))
        return view
    }


}
