package id.co.pln.intranetpln.wiki.add

import android.util.Log
import org.json.JSONObject

class WikiAddPresenter(
        val view: WikiAddView,
        val interactor: WikiAddInteractor
){
    fun addPage(){
        view.showLoading()
        interactor.createPage(
                view.getTitleWiki(),
                view.getContentWiki(),
                view.getSummaryWiki(),
                {result->onResult(result)}
        )
    }
    fun onResult(data: JSONObject?){
        view.hideLoading()
//        view.goToList()
        Log.i("DATA","Result :"+data!!.toString())
    }
}
