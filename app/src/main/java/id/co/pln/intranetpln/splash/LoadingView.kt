package id.co.pln.intranetpln.splash

interface LoadingView {
    fun hasPermission(): Boolean
    fun checkPermission()
    fun goToLogin()
    fun goToHome()
    fun showNotif()
}