package id.co.pln.intranetpln.wiki.draft

import android.app.Activity
import com.liferay.mobile.android.callback.typed.JSONArrayCallback
import com.liferay.mobile.android.v7.wikipage.WikiPageService
import id.co.pln.intranetpln.App
import id.co.pln.intranetpln.config.GROUP_ID
import id.co.pln.intranetpln.config.WIKI_NODE_ID
import id.co.pln.intranetpln.model.WikiPage
import id.co.pln.intranetpln.repository.UserPrefRepository
import io.realm.Realm
import io.realm.RealmResults
import org.json.JSONArray
import java.lang.Exception

class DraftListWikiInteractor(
        val activity: Activity
){
    var realm: Realm?=null
    val app: App?=activity.application as App
    val userRepo: UserPrefRepository = UserPrefRepository(activity)
    val wikiPageService: WikiPageService = WikiPageService(app!!.session)

    fun getDraft(callback: (JSONArray?)->Unit){
        app!!.session!!.callback=object: JSONArrayCallback(){
            override fun onSuccess(result: JSONArray?) {
                callback(result)
            }

            override fun onFailure(exception: Exception?) {
                callback(JSONArray())
            }

        }
        wikiPageService.getRecentChanges(GROUP_ID, WIKI_NODE_ID,-1,-1)
    }
    fun getDB(): Realm{
        Realm.init(activity)
        realm= Realm.getDefaultInstance()
        return realm!!
    }
    fun getDraft(): RealmResults<WikiPage>{
        Realm.init(activity)
        realm= Realm.getDefaultInstance()
        return  realm!!.where(WikiPage::class.java).notEqualTo("status","0".toLong()).findAll()
    }
}