package id.co.pln.intranetpln.main.ui

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import id.co.pln.intranetpln.main.tabs.home.HomeFragment
import id.co.pln.intranetpln.main.tabs.messages.MessageFragment
import id.co.pln.intranetpln.main.tabs.notifications.NotificationFragment
import id.co.pln.intranetpln.main.tabs.profile.ProfileFragment
import id.co.pln.intranetpln.main.tabs.tasks.TaskFragment

class MainViewPagerAdapter(fragmentManager : FragmentManager) : FragmentPagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        return when(position){
            1 -> {
                return NotificationFragment()
            }
            2 -> {
                return MessageFragment()
            }
            3 -> {
                return ProfileFragment()
            }
            4 -> {
                return TaskFragment()
            }
            else -> {
                return HomeFragment()
            }
        }
    }

    override fun getCount(): Int {
        return 5;
    }

}