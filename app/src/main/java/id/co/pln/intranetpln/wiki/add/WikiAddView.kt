package id.co.pln.intranetpln.wiki.add

interface WikiAddView{
    fun goBack()
    fun setTitle(text: String)
    fun setContent(text: String)
    fun getTitleWiki(): String
    fun getContentWiki(): String
    fun getSummaryWiki(): String
    fun showLoading()
    fun hideLoading()
    fun showError(text: String)
    fun goToHome()
    fun goToList()
}