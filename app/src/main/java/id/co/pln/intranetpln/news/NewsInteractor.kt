package id.co.pln.intranetpln.news

import android.support.v4.app.Fragment
import id.co.pln.intranetpln.App
import id.co.pln.intranetpln.repository.UserPrefRepository
import io.realm.Realm

class NewsInteractor(
        val fragment: Fragment
){
    var realm: Realm?=null
    val app: App?=fragment.activity!!.application as App
    val userRepo: UserPrefRepository = UserPrefRepository(fragment.activity!!)
    fun getAllNews(){

    }
}