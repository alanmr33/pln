package id.co.pln.intranetpln.wiki.detail

interface DetailWikiView{
    fun setWikiTitle(title: String)
    fun setWikiContent(content: String)
    fun showLoading()
    fun hideLoading()
    fun newWiki(text: String)
    fun setLastEdit(text: String)
    fun showAllComponent()
}