package id.co.pln.intranetpln.wiki

import android.util.Log
import id.co.pln.intranetpln.model.WikiPage
import org.json.JSONArray

class WikiPresenter(
        val view: WikiView,
        val interactor: WikiInteractor
){
    var data: JSONArray?=JSONArray()
    fun loadWiki(){
        view.showLoading()
        interactor.getWikiCount({total->getWikiPage(1,total)})
        interactor.getDraftCount({total->view.setDraft(total!!)})
    }
    fun getWikiPage(page: Int,totalPage: Int?){
        if(totalPage!=null){
            if(page<=totalPage){
                interactor.getWiki(page,{result->onResultWikiList(page,totalPage,result)})
            }else{
                view.hideLoading()
                view.reload(interactor.getWikiList())
            }
        }else{
            view.hideLoading()
            view.reload(interactor.getWikiList())
        }
    }
    fun onResultWikiList(page: Int,totalPage: Int?,result: JSONArray?){
        data=result
        Log.i("WIKI","data => ${data!!.length()} ")
        Log.i("WIKI","data => ${data} ")
        interactor.getDB().beginTransaction()
        interactor.getDB().createOrUpdateAllFromJson(WikiPage::class.java,result!!)
        interactor.getDB().commitTransaction()
        getWikiPage(page+1,totalPage)
    }
}
